var express = require('express');
var router = express.Router();
var fbase = require('../common/firebase_instance');
var alert = require('alert-node');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    layout: false,
    title: 'TrendsNet'
  });
});

router.get('/admin-panel', function (req, res, next) {
  res.render('admin-panel', {
    title: 'TrendsNet',
    subtitle: 'Dashboard'
  });
});

router.get('/admin-panel-users-editor', function (req, res, next) {
  res.render('admin-panel-users-editor', {
    title: 'TrendsNet',
    subtitle: 'Users'
  });
});

router.post('/admin-panel', function (req, res) {
  var username = req.body.email;
  var password = req.body.password;

  fbase.auth().signInWithEmailAndPassword(username, password).then((authenticatedUser) => {
    res.redirect('admin-panel');
  }).catch(function (error) {
    alert('Login Fail: ' + error);
    res.redirect('/');
  })
});

module.exports = router;