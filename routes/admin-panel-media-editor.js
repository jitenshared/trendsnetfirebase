var express = require('express');
var router = express.Router();
var firebase = require('../common/firebase_instance');
var fbAuth = require('../common/firebase_auth');
var dateformat = require('dateformat');
var now = new Date();
var multer = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, 'public/images');
    },
    filename: function (req, file, callback) {
        var img = {
            image_name: Date.now() + '-' + file.originalname,
            image_date: dateformat(now.toDateString(), 'yyyymmdd')
        };
        var firebasedb = firebase.database();
        firebasedb.ref('trends_images').child(dateformat(now.toDateString(), 'yyyymmdd') + '_media_editor').push().set({
            image_name: Date.now() + '-' + file.originalname,
        }).then(logs => {
            firebasedb.ref('trends_logs').child(dateformat(now.toDateString(), 'yyyymmdd') + '_media_editor').push().set({
                logs_time: dateformat(now.toDateString(), 'h:MM'),
                logs_user: 'Angelo',
                logs_action_taken: 'Uploaded an imgFile: ' + img.image_name
            });
        });

        callback(null, Date.now() + '-' + file.originalname);
    }
});

var upload = multer({
    storage: storage
}).array('images', 5);
router.get('/admin-panel-media-editor', function (req, res, next) {
    
    res.render('admin-panel-media-editor', {
        title: 'TrendsNet',
        subtitle: 'Media'
    });
    //Checks if the user is logged in
    if (fbAuth != true) {
        res.redirect('/');
    }
    else {
       
    }
});

router.post('/admin-panel-media-uploading', function (req, res, next) {
    upload(req, res, function (err) {
        if (err) {
            return res.end("Error uploading file.");
        }
    });
    console.log(req.files);
    res.send(req.files);
});

module.exports = router;