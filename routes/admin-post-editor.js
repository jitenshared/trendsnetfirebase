var express = require('express');
var router = express.Router();
var fbase = require('../common/firebase_instance');
var dateformat = require('dateformat');
var now = new Date();
var currentUser;

router.get('/admin-panel-post-editor', function (req, res, next) {

    fbase.auth().onAuthStateChanged(function (user) {
        if (user) {
            currentUser = user.displayName;
            console.log(currentUser);

            var fbasedb = fbase.database().ref().child('trends_post');

            fbase.database().ref().once('value', function (databaseChecker) {
                if (databaseChecker.hasChild('trends_post')) {

                    fbasedb.once('value', function (snapShot) {
                        var records = [];
                        var childKey;
                        snapShot.forEach(function (childSnapshot) {
                            var data = childSnapshot.val();
                            data['childKey'] = Object.keys(childSnapshot.val());
                            var key = childSnapshot.key;

                            childKey = Object.keys(childSnapshot.val());
                            console.log('CHILD KEY: ', childKey);
                            console.log('KEY: ', key);
                            console.log('DATA: ', data);

                            fbasedb.child(childSnapshot.key).once('value', function (mysnapshot) {
                                console.log(mysnapshot.key);
                                mysnapshot.forEach(function (snappp) {
                                    console.log(snappp.val());
                                    var data2 = snappp.val();
                                    data2['post_id'] = snappp.key;
                                    records.push(data2);
                                    console.log(data2);
                                });
                                res.render('admin-panel-post-editor', {
                                    records: records,
                                });
                            });
                        });
                    }).catch(error => {
                        console.log(error);
                    });

                } else {
                    res.render('admin-panel-post-editor', {
                        title: 'TrendsNet',
                        subtitle: 'Post',
                        description: 'Add your new post here'
                    });
                }
            });
        } else {
            res.redirect('/');
        }
    });
});

router.get('/admin-panel-post-text-editor', function (req, res, next) {
    res.render('admin-panel-post-text-editor', {
        title: 'TrendsNet',
        subtitle: 'Add New Post',
        description: 'Add your new post here'
    });

    fbase.auth().onAuthStateChanged(function (user) {
        if (user) {
            currentUser = user.displayName;
            console.log(currentUser);
        } else {
            res.redirect('/');
        }
    });
});

router.post('/admin-panel-post-text-editor', function (req, res, next) {
    var myDate = dateformat(now.toDateString(), 'yyyy/mm/dd');
    var ref = fbase.database().ref();
    var tp = ref.child('trends_post').child(dateformat(now.toDateString(), 'yyyymmdd') + '_post_editor');
    var postMember = tp.push().set({
        post_author: currentUser,
        post_body: req.body.body,
        post_date: myDate,
        post_title: req.body.title
    }).then(logs => {
        var logsRef = ref.child('trends_logs').child(dateformat(now.toDateString(), 'yyyymmdd') + '_post_editor');
        logsRef.push().set({
            logs_time: dateformat(now.toDateString(), 'h:MM'),
            logs_date: myDate,
            logs_user: currentUser,
            logs_action_taken: 'Posted a post titled: ' + req.body.title
        }).catch(function (error) {
            console.log('ERROR: ' + error);
        });
    }).catch(error => {
        console.log('ERROR: ' + error);
    });
    res.render('admin-panel-post-text-editor', {
        title: 'TrendsNet',
        subtitle: 'Post'
    });
});

module.exports = router;