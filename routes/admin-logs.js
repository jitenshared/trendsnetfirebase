var express = require('express');
var router = express.Router();
var fbase = require('../common/firebase_instance');
var currentUser;


router.get('/admin-panel-logs-editor', function (req, res, next) {

  fbase.auth().onAuthStateChanged(function (user) {
    if (user) {
      currentUser = user.displayName;
      console.log(currentUser);

      var fbasedb = fbase.database().ref().child('trends_logs');

      fbase.database().ref().once('value', function (databaseChecker) {
        if (databaseChecker.hasChild('trends_logs')) {

          fbasedb.once('value', function (snapShot) {
            var records = [];
            var childKey;
            snapShot.forEach(function (childSnapshot) {
              var data = childSnapshot.val();
              data['childKey'] = Object.keys(childSnapshot.val());
              var key = childSnapshot.key;

              childKey = Object.keys(childSnapshot.val());
              console.log('CHILD KEY: ', childKey);
              console.log('KEY: ', key);
              console.log('DATA: ', data);

              fbasedb.child(childSnapshot.key).once('value', function (mysnapshot) {
                console.log(mysnapshot.key);
                mysnapshot.forEach(function (snappp) {
                  console.log(snappp.val());
                  var data2 = snappp.val();
                  data2['logs_id'] = snappp.key;
                  records.push(data2);
                  console.log(data2);
                });
                res.render('admin-panel-logs-editor', {
                  records: records,
                });
              });
            });
          }).catch(error => {
            console.log(error);
          });

        } else {
          res.render('admin-panel-logs-editor', {
            title: 'TrendsNet',
            subtitle: 'logs',
            description: 'Add your new logs here'
          });
        }
      });

    } else {
      res.redirect('/');
    }
  });
});


module.exports = router;