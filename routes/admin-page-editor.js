var express = require('express');
var router = express.Router();
var fbase = require('../common/firebase_instance');
var dateformat = require('dateformat');
var now = new Date();
var currentUser;


router.get('/admin-panel-page-editor', function (req, res, next) {
  //Checks if the user is logged in
  fbase.auth().onAuthStateChanged(function (user) {
    if (user) {
      currentUser = user.displayName;
      console.log('PAGE_USER-------------------->', currentUser);

      var fbasedb = fbase.database().ref().child('trends_pages');

      fbase.database().ref().once('value', function (databaseChecker) {


        if (databaseChecker.hasChild('trends_pages')) {
          fbasedb.once('value', function (snapShot) {
            var records = [];
            var childKey;
            snapShot.forEach(function (childSnapshot) {
              var data = childSnapshot.val();
              data['childKey'] = Object.keys(childSnapshot.val());
              var key = childSnapshot.key;

              childKey = Object.keys(childSnapshot.val());
              console.log('CHILD KEY: ', childKey);
              console.log('KEY: ', key);
              console.log('DATA: ', data);

              fbasedb.child(childSnapshot.key).once('value', function (mysnapshot) {
                console.log(mysnapshot.key);
                mysnapshot.forEach(function (snappp) {
                  console.log(snappp.val());
                  var data2 = snappp.val();
                  data2['page_id'] = snappp.key;
                  records.push(data2);
                  console.log(data2);
                });
                res.render('admin-panel-page-editor', {
                  records: records,
                });
              });
            });
          });

        } else {
          res.render('admin-panel-page-editor', {
            title: 'TrendsNet',
            subtitle: 'Pages'
          });
        }
      });

    } else {
      res.redirect('/');
    }
  });

});

router.get('/admin-panel-page-text-editor', function (req, res, next) {
  res.render('admin-panel-page-text-editor', {
    title: 'TrendsNet',
    subtitle: 'Pages'
  });
  //Checks if the user is logged in
  fbase.auth().onAuthStateChanged(function (user) {
    if (user) {
      currentUser = user.displayName;
      console.log('PAGE_USER-------------------->', currentUser);
    } else {
      res.redirect('/')
    }
  });
});

router.post('/admin-panel-page-text-editor', function (req, res) {
  var myDate = dateformat(now.toDateString(), 'yyyy/mm/dd');
  var ref = fbase.database().ref();
  var tp = ref.child('trends_pages').child(dateformat(now.toDateString(), 'yyyymmdd') + '_page_editor');
  var pagesMember = tp.push().set({
    page_author: currentUser,
    page_body: req.body.body,
    page_date: myDate,
    page_title: req.body.title
  }).then(logs => {
    var logsRef = ref.child('trends_logs').child(dateformat(now.toDateString(), 'yyyymmdd') + '_page_editor');
    logsRef.push().set({
      logs_time: dateformat(now.toDateString(), 'h:MM'),
      logs_date: myDate,
      logs_user: currentUser,
      logs_action_taken: 'Posted a page titled: ' + req.body.title
    }).catch(function (error) {
      console.log('ERROR: ' + error);
    });
  }).catch(error => {
    console.log('ERROR: ' + error);
  });
  res.render('admin-panel-page-text-editor', {
    title: 'TrendsNet',
    subtitle: 'Pages'
  });
});

module.exports = router;