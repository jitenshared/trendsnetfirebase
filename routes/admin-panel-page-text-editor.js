var express = require('express');
var router = express.Router();
var fbase = require('../common/firebase_instance');
var dateformat = require('dateformat');
var now = new Date();
var currentUser;
router.get('/admin-panel-page-text-editor', function (req, res, next) {


  res.render('admin-panel-page-text-editor', {
    title: 'TrendsNet',
    subtitle: 'Pages'
  });
  //Checks if the user is logged in
  fbase.auth().onAuthStateChanged(function (user) {
    if (user) {
      currentUser = user.displayName;
      console.log('PAGE_USER-------------------->', currentUser);
    } else {
      res.render('/admin-panel-page-editor')
    }
  });

});

router.post('/admin-panel-page-text-editor', function (req, res) {
  var ref = fbase.database().ref();
  var tp = ref.child('trends_pages');
  var pagesMember = tp.push().set({
    page_author: currentUser,
    page_body: req.body.body,
    page_date: dateformat(now.toDateString(), 'yyyymmdd'),
    page_title: req.body.title
  }).then(logs => {
    var logsRef = ref.child('trends_logs').child(dateformat(now.toDateString(), 'yyyymmdd') + '_page_editor');
    logsRef.push().set({
      logs_time: dateformat(now.toDateString(), 'h:MM'),
      logs_user: currentUser,
      logs_action_taken: 'Posted a page titled: ' + req.body.title
    });
  }).catch(error => {
    console.log('ERROR: ' + error);
  });
  res.render('admin-panel-page-text-editor', {
    title: 'TrendsNet',
    subtitle: 'Pages'
  });
});

module.exports = router;