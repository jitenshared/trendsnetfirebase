var express = require('express');
var router = express.Router();
var fbase = require('../common/firebase_instance');
var dateformat = require('dateformat');
var now = new Date();
var alert = require('alert-node');
//Test
var admin = require('../common/firebase_admin_instance');
var currentUser = fbase.auth().currentUser;

router.get('/admin-register', function (req, res, next) {
  res.render('admin-register', {
    layout: false,
    title: 'TrendsNet'
  });
});

router.post('/', function (req, res) {
  var fbaseReference = fbase.database().ref();
  var userRef = fbaseReference.child('trends_users');
  var logsRef = fbaseReference.child('trends_logs');

  var userEmail = req.body.email;
  var userPassword = req.body.password;
  var userFullName = req.body.fullname;
  console.log(userEmail);

  console.log(userPassword);

  fbase.auth().createUserWithEmailAndPassword(userEmail, userPassword).then((newUser) => {
    fbase.auth().signInWithEmailAndPassword(userEmail, userPassword).then((authenticatedUser) => {
      userRef.child(authenticatedUser.uid).set({
        full_name: userFullName,
        email: userEmail
      }).then((leggoUpdateUserProf) => {
        fbase.auth().onAuthStateChanged(function (user) {
          if (user) {
            user.updateProfile({
              displayName: userFullName
            }).then(function () {
              console.log('Update Success!');
            });
          }
        });
      });
      res.redirect('/');
    }).catch(function (error) {
      alert('ERROR: ' + error);
      res.redirect('admin-register');
    });
  }).catch(function (error) {
    alert('REGISTRATION FAIL: ' + error)
    res.redirect('admin-register');
  });
});
module.exports = router;

// .then(function (token) {
//   fbase.auth().onAuthStateChanged(function (user) {
//     if (user) {
//       user.getIdToken().then(function (data) {
//         console.log('TOKEN: ' + data);
//       });
//     }
//   });
// });