var admin = require("firebase-admin");

var serviceAccount = require("../trendsnet-service-key.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://trendsnet-8ae79.firebaseio.com"
});
