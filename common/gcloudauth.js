var gcloud = require('gcloud');

var storage = gcloud.storage({
  projectId: 'trendsnet-90b5b',
  keyFilename: '../trendsnet-service-key.json'
});

var bucket = storage.bucket('trendsnet-90b5b.appspot.com');